<?php

/**
 * Implements hook_rules_action_info().
 */
function snapi_fb_rules_action_info() {
	$actions = array(
		'snapi_fb_action_post_fbpost' => array(
			'base' => 'snapi_fb_action_post_fbpost',
			'label' => t('Post an entity to Facebook'),
			'group' => t('Social Network API'),
			'parameter' => array(
				'value' => array(
					'type' => 'text',
					'label' => t('Facebook post format.'),
					'description' => t('Insert template of your post on Facebook.'),
					'restriction' => 'input',
				),
			),
		),
	);
	
	return $actions;
}

/**
 * Rules action for debugging values.
 */


function snapi_fb_action_post_fbpost($value) {

	drupal_set_message(t($value));

}