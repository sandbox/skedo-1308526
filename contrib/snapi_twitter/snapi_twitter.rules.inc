<?php

/**
 * Implements hook_rules_action_info().
 */
function snapi_twitter_rules_action_info() {
	$actions = array(
		'snapi_twitter_action_post_fbpost' => array(
			'base' => 'snapi_twitter_action_tweet',
			'label' => t('Post an entity to Twitter'),
			'group' => t('Twitter'),
			'parameter' => array(
				'value' => array(
					'type' => 'text',
					'label' => t('Tweet post format.'),
					'description' => t('Insert template of your post on Twitter.'),
					'restriction' => 'input',
				),
				/*'label' => array(
					'type' => 'unknown',
					'label' => t('Twitter Account'),
					'description' => 'Twitter account used',
					'restriction' => 'input',
				),*/
			),
		),
	);
	
	return $actions;
}

/**
 * Rules action for debugging values.
 */


function snapi_twitter_action_tweet($value) {
	include_once (drupal_get_path('module', 'snapi_twitter') . '/lib/twitteroauth.php');
	$token = FALSE;
	$access_token_row = db_query('SELECT a.oauth_token, a.oauth_token_secret, a.consumer_key, a.consumer_secret FROM {snapi_twitter_tokens} a WHERE a.uid = 0');
	
	foreach( $access_token_row as $atr ) {
		$token = array(
			'key'     => $atr->oauth_token,
			'secret'  => $atr->oauth_token_secret,
			'consumer_key' => $atr->consumer_key,
			'consumer_secret' => $atr->consumer_secret,
		);
	}
	
	$connection = new TwitterOAuth($token['consumer_key'], $token['consumer_secret'], $token['key'], $token['secret']);
	$user = $connection->get('account/verify_credentials');
	
	$parameters = array('status' => $value);
	$status = $connection->post('statuses/update', $parameters);

	drupal_set_message(t($status));

}